import React from "react";

import "./Footer.sass";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Link } from "../../../shared/components/Link";
import { Logo } from "../../../shared/components/Logo";

export const Footer = () => {

    const footerInfo = ["About", "Terms of Service", "Contact"];

    const footerInfoItems = footerInfo.map(elem => {
        
        return (
            <Link text={elem} />
        )
    })

    return (
        <div className="footer">
            <div className="footer-part-one">
                <div className="footer-navi">
                    {footerInfoItems}
                </div>
                <Logo color="#0aaee4" />
                <div className="footer-icons">
                    <FontAwesomeIcon icon={['fab', 'facebook-f']} className="footer-icons-icon" />
                    <FontAwesomeIcon icon={['fab', 'twitter']} className="footer-icons-icon" />
                    <FontAwesomeIcon icon={['fab', 'pinterest-p']} className="footer-icons-icon" />
                    <FontAwesomeIcon icon={['fab', 'instagram']} className="footer-icons-icon" />
                    <FontAwesomeIcon icon={['fab', 'youtube']} className="footer-icons-icon" />
                </div>
            </div>
            <p className="footer-part-two">
                Copyright. &copy; 2017 <span>Movie</span><span>Rise</span>. All Rights Reserved.
            </p>
        </div>
    )
} 