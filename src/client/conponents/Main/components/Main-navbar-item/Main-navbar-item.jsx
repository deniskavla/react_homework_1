import React from "react";

import "./Main-navbar-item.sass";

export const MainNavbarItem = (props) => {

    return (

        <a className="main-navbar-item">
            <h4>{props.text}</h4>
        </a>
    )
}