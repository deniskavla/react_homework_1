import React from "react";

import "./Main-item.sass";

export const MainItem = (props) => {

    return (
        <div className="main-item">
            <img src={props.poster} alt="Pic" />
            <div className="main-item-info">
                <div className="main-item-info-tm">
                    <h4 className="main-item-info-tm-title">
                        {props.title}
                    </h4>
                    <h4 className="main-item-info-tm-mark">
                        {props.mark}
                    </h4>
                </div>
                <small className="main-item-info-ganres">
                    {props.genres}
                </small>
            </div>
        </div>
    )
} 