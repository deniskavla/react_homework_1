import React from "react";

import "./Link.sass";

export const Link = (props) => {

    return (
        <a className="link" href="#">
            {props.text}
        </a>
    )
}